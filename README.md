# CASE Validation Example

_This is an unofficial GitHub Action and not endorsed by the CASE Community or the Linux Foundation. No warranties or guarantees are made to its accuracy or functionality._

## Overview

This template is to highlight example usage of the [CASE Validation Action]((https://github.com/marketplace/actions/case-ontology-validator)) that is primarily focused as a GitHub Action and is available at: [https://github.com/kchason/case-validation-action](https://github.com/kchason/case-validation-action).

This repository utilizes the Docker image that is available on Docker Hub: [https://hub.docker.com/r/kchason/case-validator](https://hub.docker.com/r/kchason/case-validator)

## Usage

### GitLab Components

This project creates a GitLab [component](https://docs.gitlab.com/ee/ci/components/) that can be used to add jobs to the pipeline.

```yaml
include:
  - component: https://gitlab.com/keith.chason/case-validation-example/case-validate@CI_COMMIT_SHA
    inputs:
      case-path: tests/data
```

The full list of variables that can be configured and their defaults are:

| Variable           | Description | Default |
|--------------------|-------------------------------------|---------|
| `case-path`        | The path to the file or directory to be validated. This can be relative to the project or an absolute path. | `/opt/json` |
| `case-version`     | The version of the ontology against which the graph should be validatated. | `case-1.3.0` |
| `filter-extension` | The extension of only the files against which the validator should be run. Eg. "json", "jsonld", "case". Defaults to "" to run against all files defined in CASE_PATH. | `""` |
| `runner-tag`       | The tag of the runner to use for the job (must be a docker or kubernetes executor). | `docker` |
| `stage`            | The stage in the pipeline in which to run the job. | `validate` |
| `job-title`        | The title of the job. This must be overridden if the job is included multiple times. | `CASE Validation` |

### Custom Integration

The built container image available on Docker Hub can also be integrated into [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) to validate files in the source repository.

This job depends on a runner tagged with `docker` which is a default in GitLab's hosted [SaaS runners](https://docs.gitlab.com/ee/ci/runners/) but can be modified for self-hosted environments so long as it's a Docker or Kubernetes [executor](https://docs.gitlab.com/runner/#executors).

Full documentation is available at the main project repository on GitHub: [https://github.com/kchason/case-validation-action](https://github.com/kchason/case-validation-action)

Examples are available in this project's [`.gitlab-ci.yml`](https://gitlab.com/keith.chason/case-validation-example/-/blob/main/.gitlab-ci.yml)

```yaml
CASE Export Validation:
    stage: Validation
    allow_failure: false
    image: kchason/case-validator:1.5.0
    variables:
        # The path to the file or directory to be validated. This can be relative to the project or an absolute path.
        CASE_PATH: "tests/data"
        # The version of the ontology against which the graph should be validatated.
        CASE_VERSION: "case-1.3.0"
        # The extension of only the files against which the validator should be run. Eg. "json", "jsonld", "case". Defaults to "" to run against all files defined in CASE_PATH.
        FILTER_EXTENSION: "jsonld"
    script:
        # This script is provided in the Docker image and processes the validation based on the environment variables specified in the job
        - source /opt/workspace/entrypoint.sh
    tags:
        - docker
```